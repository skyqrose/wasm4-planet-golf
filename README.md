# Planet Golf

A game written in Rust for the [WASM-4](https://wasm4.org) fantasy console.

## Building

1. [Install WASM-4](https://wasm4.org/docs/getting-started/setup?code-lang=rust#quickstart) and Rust.
2. Build the cart:  `cargo build --release`
3. `w4 run target/wasm32-unknown-unknown/release/cart.wasm`
