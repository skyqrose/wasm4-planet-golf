use std::ops;

#[derive(Clone, Copy, Debug)]
pub struct V2 {
    pub x: f32,
    pub y: f32,
}
impl ops::Add<V2> for V2 {
    type Output = V2;
    fn add(self, rhs: V2) -> V2 {
        V2 {
            x: self.x + rhs.x,
            y: self.y + rhs.y,
        }
    }
}
impl ops::AddAssign<V2> for V2 {
    fn add_assign(&mut self, rhs: V2) {
        self.x += rhs.x;
        self.y += rhs.y;
    }
}
impl ops::Sub<V2> for V2 {
    type Output = V2;
    fn sub(self, rhs: V2) -> V2 {
        V2 {
            x: self.x - rhs.x,
            y: self.y - rhs.y,
        }
    }
}
impl ops::SubAssign<V2> for V2 {
    fn sub_assign(&mut self, rhs: V2) {
        self.x -= rhs.x;
        self.y -= rhs.y;
    }
}
impl ops::Mul<f32> for V2 {
    type Output = V2;
    fn mul(self, rhs: f32) -> V2 {
        V2 {
            x: self.x * rhs,
            y: self.y * rhs,
        }
    }
}
impl V2 {
    pub fn mag2(&self) -> f32 {
        self.x * self.x + self.y * self.y
    }
}

pub const ZERO: V2 = V2 { x: 0.0, y: 0.0 };
