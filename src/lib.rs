#[cfg(feature = "buddy-alloc")]
mod alloc;
mod flag;
mod planet;
mod v2;
mod wasm4;
use planet::Planet;
use std::f32::consts::TAU;
use v2::V2;
use wasm4::*;

/* coordinate space:
origin 0.0, 0.0 is in the center of the screen.
1.0 in coordinate space is 1px in screen space.
positive y is at the top of the screen (screen y = 0)
postive x is at the right of the screen (screen x = 160)
*/

struct Level {
    planets: &'static [Planet],
    start_pos: V2,
    flag_pos: V2,
    flag_direction: flag::Direction,
}

struct Data {
    levels: &'static [Level],
}

static DATA: Data = Data {
    levels: &[
        Level {
            planets: &[
                Planet {
                    pos: V2 { x: 40.0, y: 0.0 },
                    r: 20.0,
                    acc: 300.0,
                },
                Planet {
                    pos: V2 { x: -40.0, y: 0.0 },
                    r: 15.0,
                    acc: 200.0,
                },
            ],
            start_pos: V2 { x: 0.0, y: 60.0 },
            flag_pos: V2 { x: 60.0, y: 0.0 },
            flag_direction: flag::Direction::Right,
        },
        Level {
            planets: &[
                Planet {
                    pos: V2 { x: 40.0, y: -10.0 },
                    r: 10.0,
                    acc: 200.0,
                },
                Planet {
                    pos: V2 { x: -40.0, y: -10.0 },
                    r: 10.0,
                    acc: 200.0,
                },
                Planet {
                    pos: V2 { x: -0.0, y: 30.0 },
                    r: 10.0,
                    acc: 200.0,
                },
            ],
            start_pos: V2 { x: -40.0, y: 20.0 },
            flag_pos: V2 { x: 0.0, y: 20.0 },
            flag_direction: flag::Direction::Down,
        },
    ],
};

#[derive(Clone, Copy, PartialEq, Debug)]
enum GameState {
    Moving,
    Landed,
    Win,
}

#[derive(Clone, Copy, Debug)]
struct State {
    level: usize,
    pos: V2,
    vel: V2,
    game_state: GameState,
    planet_index: u8,
    // angle from center of planet to ball, in turns, counterclockwise from right
    planet_angle: f32,
    // turns from normal, negative is left, pos is right.
    aim_angle: f32,
    aim_strength: f32,
    // used to prevent double launching if the ball lands shortly after launch
    holding_x: bool,
}

fn load_level(state: &mut State, i: usize) {
    state.level = i;
    state.pos = DATA.levels[i].start_pos;
    state.vel = V2 { x: 0.0, y: 0.0 };
    state.game_state = GameState::Moving;
    // prevent unexpected launching on initial load
    state.holding_x = true;
}

// values not important, they're all initialized before use anyway
static mut STATE: State = State {
    level: 0,
    pos: v2::ZERO,
    vel: v2::ZERO,
    game_state: GameState::Moving,
    planet_index: 0,
    planet_angle: 0.0,
    aim_angle: 0.0,
    aim_strength: 1.0,
    holding_x: true,
};

#[no_mangle]
fn start() {
    unsafe {
        load_level(&mut STATE, 0);
        // *PALETTE = [0xfff6d3, 0xf9a875, 0xeb6b6f, 0x7c3f58];
    }
}

#[no_mangle]
fn update() {
    unsafe {
        update_state_input(&mut STATE, *GAMEPAD1);
        update_state_time(&mut STATE);
        draw(&STATE);
    }
}

fn update_state_input(state: &mut State, gamepad: u8) {
    // launch
    if state.game_state == GameState::Landed {
        if (gamepad & BUTTON_1 != 0) && !state.holding_x {
            state.vel = aim_unit(state) * state.aim_strength;
            state.game_state = GameState::Moving;
        }
    }

    // aiming
    if state.game_state == GameState::Landed {
        if gamepad & BUTTON_LEFT != 0 {
            state.aim_angle -= 1.0 / 360.0;
        }
        if gamepad & BUTTON_RIGHT != 0 {
            state.aim_angle += 1.0 / 360.0;
        }
        if gamepad & BUTTON_UP != 0 {
            state.aim_strength += 0.1;
        }
        if gamepad & BUTTON_DOWN != 0 {
            state.aim_strength -= 0.1;
        }
        state.aim_angle = state.aim_angle.clamp(-0.25, 0.25);
        state.aim_strength = state.aim_strength.clamp(1.2, 7.0);
    }

    // next level
    if state.game_state == GameState::Win {
        if (gamepad & BUTTON_1 != 0) && !state.holding_x {
            load_level(state, (state.level + 1) % DATA.levels.len());
        }
    }

    //reset
    if gamepad & BUTTON_2 != 0 {
        load_level(state, state.level);
    }
    state.holding_x = gamepad & BUTTON_1 != 0;
}

fn update_state_time(state: &mut State) {
    match state.game_state {
        GameState::Landed => {
            // noop
        }
        GameState::Moving => {
            let planets = &DATA.levels[state.level].planets;
            let planet_i_option = physics(&mut state.pos, &mut state.vel, planets);
            if let Some(i) = planet_i_option {
                hit_ground(state, i as u8, &planets[i]);
            }
        }
        GameState::Win => {
            // noop
        }
    }
}

/// returns the index of the planet you hit
fn physics(pos: &mut V2, vel: &mut V2, planets: &[Planet]) -> Option<usize> {
    *pos += *vel;
    for (i, planet) in planets.iter().enumerate() {
        let hit_planet = planet.apply_gravity(pos, vel);
        if hit_planet {
            return Some(i);
        }
    }
    return None;
}

fn hit_ground(state: &mut State, planet_index: u8, planet: &Planet) {
    let level = &DATA.levels[state.level];
    planet.move_to_surface(&mut state.pos);
    state.vel = V2 { x: 0.0, y: 0.0 };
    if (state.pos - level.flag_pos).mag2() < flag::HOLE_SIZE2 {
        state.game_state = GameState::Win;
    } else {
        state.game_state = GameState::Landed;
        state.planet_index = planet_index;
        state.planet_angle = planet.normal_angle(&state.pos);
        state.aim_angle = 0.0;
        state.aim_strength = 5.0;
    }
}

fn draw(state: &State) {
    let level = &DATA.levels[state.level];
    fill_4();
    unsafe {
        // draw planets
        *DRAW_COLORS = 0x0032;
        for planet in level.planets {
            planet.draw();
        }
        flag::draw_flag(level.flag_pos, level.flag_direction);
        // draw ball
        *DRAW_COLORS = 0x0001;
        draw_square(state.pos, 1.5);
        if state.game_state == GameState::Landed {
            draw_trajectory(state);
        }
        if state.game_state == GameState::Win {
            text(
                "You Win!\nPrex x for the next level\nPress z to replay",
                0,
                0,
            );
        }
    }
}

fn draw_trajectory(state: &State) {
    let level = &DATA.levels[state.level];
    let mut pos = state.pos;
    let mut vel = aim_unit(state) * state.aim_strength;
    for _ in 0..4 {
        for _ in 0..3 {
            let hit_planet = physics(&mut pos, &mut vel, level.planets);
            if hit_planet.is_some() {
                return;
            }
        }
        draw_square(pos, 0.5);
    }
}

fn draw_square(c: V2, r: f32) {
    let left = (80.0 + c.x - r).round() as i32;
    let top = (80.0 - c.y - r).round() as i32;
    let wh = (r * 2.0).round() as u32;
    rect(left, top, wh, wh);
}

fn fill_4() {
    unsafe {
        (&mut *FRAMEBUFFER).fill(3 | (3 << 2) | (3 << 4) | (3 << 6));
    }
}

fn aim_unit(state: &State) -> V2 {
    let angle = state.planet_angle - state.aim_angle;
    let (sin, cos) = f32::sin_cos(angle * TAU);
    V2 { x: cos, y: sin }
}
