use crate::v2::V2;
use crate::wasm4::*;

/// threshold for what counts as getting the ball in the hole
/// squared, for comparing to the distance squared
pub const HOLE_SIZE2: f32 = 10.0;

const FLAG_WIDTH: u32 = 5;
const FLAG_HEIGHT: u32 = 8;
/*
31003
31000
31003
31333
31333
31333
33333
13331

origin is middle col, 2nd to last row
*/
const FLAG: [u8; 10] = [
    0b11010000, 0b11110100, 0b00001101, 0b00001111, 0b01111111, //
    0b11011111, 0b11110111, 0b11111111, 0b11111101, 0b11111101,
];

#[derive(Clone, Copy, Debug)]
pub enum Direction {
    Up,
    Left,
    Right,
    Down,
}

pub fn draw_flag(pos: V2, direction: Direction) {
    let px = 80 + pos.x as i32;
    let py = 80 - pos.y as i32;
    unsafe {
        *DRAW_COLORS = 0x4321;
    }
    let (x, y, flags) = match direction {
        Direction::Up => (px - 2, py - 6, 0),
        Direction::Left => (px - 6, py - 2, BLIT_2BPP | BLIT_ROTATE),
        Direction::Right => (px - 1, py - 2, BLIT_ROTATE | BLIT_FLIP_X | BLIT_FLIP_Y),
        Direction::Down => (px - 2, py - 1, BLIT_FLIP_X | BLIT_FLIP_Y),
    };
    blit(&FLAG, x, y, FLAG_WIDTH, FLAG_HEIGHT, BLIT_2BPP | flags);
}
