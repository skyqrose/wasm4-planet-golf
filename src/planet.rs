use crate::v2::V2;
use crate::wasm4;
use std::f32::consts::TAU;

pub struct Planet {
    pub pos: V2,
    pub r: f32,
    pub acc: f32,
}

impl Planet {
    /// updates vel
    /// returns true if the ball hit the planet
    pub fn apply_gravity(&self, pos: &V2, vel: &mut V2) -> bool {
        // v2 from planet to ball
        let d = *pos - self.pos;
        let r2 = d.mag2();
        if r2 < self.r * self.r {
            // hit planet
            *vel = V2 { x: 0.0, y: 0.0 };
            return true;
        } else {
            // no collision, apply acceleration
            let r3 = f32::powf(r2, -1.5);
            *vel -= d * (self.acc * r3);
            return false;
        }
    }

    /// mutates pos
    pub fn move_to_surface(&self, pos: &mut V2) {
        let d = *pos - self.pos;

        // +epsilon so it's outside the planet
        let r = self.r + 0.5;
        *pos = self.pos + d * (r / d.mag2().sqrt());
    }

    pub fn normal_angle(&self, pos: &V2) -> f32 {
        let d = *pos - self.pos;
        return f32::atan2(d.y, d.x) * (1.0 / TAU);
    }

    pub fn draw(&self) {
        draw_circle(self.pos, self.r);
    }
}

fn draw_circle(c: V2, r: f32) {
    let left = (80.0 + c.x - r).round() as i32;
    let top = (80.0 - c.y - r).round() as i32;
    let wh = (r * 2.0).round() as u32;
    wasm4::oval(left, top, wh + 1, wh + 1);
}
